package edu.calpoly.controller;

import edu.calpoly.view.MenuItemView;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class MenuListAdapter extends BaseAdapter {
	
	private Context m_context;
	private String[] m_title;
    private String[] m_subTitle;
    private int[] m_icon;
	
    public MenuListAdapter(Context context, String[] title, String[] subtitle,
            int[] icon) {
    	this.m_context = context;
        this.m_title = title;
        this.m_subTitle = subtitle;
        this.m_icon = icon;
    }
    
    @Override
    public int getCount() {
        return m_title.length;
    }
 
    @Override
    public String getItem(int position) {
        return m_title[position];
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
    	if (convertView == null) {
			convertView = new MenuItemView(this.m_context, m_title[position], m_subTitle[position], m_icon[position]);
		}
		else {
			((MenuItemView) convertView).setMenuItem(m_title[position], m_subTitle[position], m_icon[position]);
		}
		return convertView;
    }

}
