package edu.calpoly.controller;

import java.util.List;

import edu.calpoly.model.Message;
import edu.calpoly.view.MessageListItemView;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class MessageListAdapter extends BaseAdapter {
	private Context m_context;
	private List<Message> m_messageList;
	
	public MessageListAdapter(Context context, List<Message> list) {
		this.m_context = context;
		this.m_messageList = list;
	}
	
	@Override
	public int getCount() {
		return m_messageList.size();
	}

	@Override
	public Object getItem(int arg0) {
		return m_messageList.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = new MessageListItemView(m_context, m_messageList.get(position));
		}
		else {
			((MessageListItemView) convertView).setMessage(m_messageList.get(position));
		}
		return convertView;
	}

}
