package edu.calpoly.controller;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import edu.calpoly.model.*;
import edu.calpoly.view.UserListItemView;

public class SearchListAdapter extends BaseAdapter {
	private Context m_context;
	private List<PicscoveryUser> m_userList;
	
	public SearchListAdapter(Context context, List<PicscoveryUser> list) {
		this.m_context = context;
		this.m_userList = list;
	}
	
	@Override
	public int getCount() {
		return m_userList.size();
	}

	@Override
	public PicscoveryUser getItem(int arg0) {
		return m_userList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = new UserListItemView(m_context, m_userList.get(position));
		}
		else {
			((UserListItemView) convertView).setPicscoveryUser(m_userList.get(position));
		}
		return convertView;
	}

}
