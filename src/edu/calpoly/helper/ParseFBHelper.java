package edu.calpoly.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.util.Log;

import com.facebook.model.GraphUser;
import com.parse.ParseObject;
import com.parse.ParseUser;

import edu.calpoly.model.Message;
import edu.calpoly.model.PicscoveryUser;
import edu.calpoly.view.PicscoveryApp;

public class ParseFBHelper {

	public static ParseUser fbToParseUser(GraphUser user) { 
		ParseUser currentUser = null;
		try {
			JSONObject userProfile = new JSONObject();
			// Populate the JSON object
			userProfile.put("facebookId", user.getId());
			userProfile.put("name", user.getName());
			if (user.getLocation().getProperty("name") != null) {
				userProfile.put("location", (String) user.getLocation().getProperty("name"));
			}
			if (user.getProperty("gender") != null) {
				userProfile.put("gender", (String) user.getProperty("gender"));
			}
			if (user.getBirthday() != null) {
				userProfile.put("birthday", user.getBirthday());
			}
			if (user.getProperty("relationship_status") != null) {
				userProfile.put("relationship_status", (String) user.getProperty("relationship_status"));
			}
			
			// Save the user profile info in a user property
			currentUser = ParseUser.getCurrentUser();
			currentUser.put("profile", userProfile);
		} catch (Exception e) {
			Log.d(PicscoveryApp.TAG, "Error parsing returned user data.");
		}
		
		return currentUser;
	}
	
	@SuppressLint("SimpleDateFormat")
	private static int dateToAge(String birthDateStr) {
		int age = 0;
		
		try {
			Date birthDate = new SimpleDateFormat("MM/dd/yyyy").parse(birthDateStr);
			Calendar birthCalendar = Calendar.getInstance();
			birthCalendar.setTime(birthDate);
			
			Calendar curCalendar = Calendar.getInstance();
			
			int dayRemainder = curCalendar.get(Calendar.DAY_OF_MONTH) - birthCalendar.get(Calendar.DAY_OF_MONTH);
			int monthRemainder = (curCalendar.get(Calendar.MONTH) + 1) - (birthCalendar.get(Calendar.MONTH) + 1);
			age = (curCalendar.get(Calendar.YEAR) - birthCalendar.get(Calendar.YEAR));
			
			if (monthRemainder <= 0) {
				age += (dayRemainder >= 0) ? 0 : -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return age;
	}
	
	public static PicscoveryUser parseToPicscoveryUser(ParseUser user) {
		Log.d(PicscoveryApp.TAG, "Creating picscovery user");
		PicscoveryUser picUser = new PicscoveryUser();
		if (user.get("profile") != null) {
			JSONObject userProfile = user.getJSONObject("profile");
			try {
				if (userProfile.getString("facebookId") != null) {
					picUser.setFacebookID(userProfile.get("facebookId").toString());
				} 
				if (userProfile.getString("name") != null) {
					Scanner s = new Scanner(userProfile.getString("name"));
					picUser.setName(s.next());
				} 
				if (userProfile.getString("gender") != null) {
					picUser.setGender(userProfile.getString("gender"));
				}
				if (userProfile.getString("birthday") != null) {
					picUser.setAge(dateToAge(userProfile.getString("birthday")));
				}
				if (userProfile.getString("location") != null) {
					String hometown = userProfile.getString("location");
					hometown = hometown.substring(0, hometown.indexOf(',')).trim();
					picUser.setHometown(hometown);
				}
			} catch (JSONException e) {
				Log.d(PicscoveryApp.TAG, "Error parsing saved user data: " + e.getMessage());
			}
		}
		
		return picUser;
	}
	
	@SuppressLint("SimpleDateFormat")
	public static Message parseToPicscoveryMessage(ParseObject object) {
		Message message = new Message();
		
		if (object.getString("caption") != null) {
			message.setCaption(object.getString("caption"));
		}
		if (object.getBytes("image") != null) {
			message.setPictureData(object.getBytes("image"));
		}
		if (object.getString("recipientFbId") != null) {
			message.setRecipientFacebookId(object.getString("recipientFbId"));
		}
		if (object.getString("recipientName") != null) {
			message.setRecipientName(object.getString("recipientName"));
		}
		if (object.getString("senderFbId") != null) {
			message.setSenderFacebookId(object.getString("senderFbId"));
		}
		if (object.getString("senderName") != null) {
			message.setSenderName(object.getString("senderName"));
		}
		if (object.getCreatedAt() != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
			SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
			String dateStr = dateFormat.format(object.getCreatedAt());
			String timeStr = timeFormat.format(object.getCreatedAt());
			message.setDateSent(dateStr + " at " + timeStr);
		}
		
		return message;
	}
	
	public static boolean sendMessage(PicscoveryUser sender, PicscoveryUser recipient, byte[] image, String caption) {
		ParseObject messageObject = new ParseObject("Messages");
		
		messageObject.put("recipientName", recipient.getName());
		messageObject.put("recipientFbId", recipient.getFacebookID());
		messageObject.put("senderName", sender.getName());
		messageObject.put("senderFbId", sender.getFacebookID());
		messageObject.put("image", image);
		messageObject.put("caption", caption); 
		 
		messageObject.saveInBackground();
		
		return true; 
	}
}
