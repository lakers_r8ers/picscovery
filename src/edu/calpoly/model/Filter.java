package edu.calpoly.model;

public class Filter {
	private String user_gender;
	private double user_distance;
	
	public Filter(String gender, double distance){
		user_gender = gender;
		user_distance = distance;
	}
	
	public String getGender() {
		return user_gender;		
	}
	
	public void setGender(String gender){
		user_gender = gender;
	}
	
	public double getDistance() {
		return user_distance;
	}
	
	public void setDistance(double distance) {
		user_distance = distance;
	}
	
}
