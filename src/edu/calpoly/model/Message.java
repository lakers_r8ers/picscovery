package edu.calpoly.model;

public class Message {
	private String caption;
	private String recipientName;
	private String senderName;
	private String recipientFacebookId;
	private String senderFacebookId;
	private String dateSent;
	private boolean isOpen;
	private byte[] pictureData;
	
	public Message() {
		
	}
	
	public Message(String caption, String recipientName, String senderName, 
			String dateSent, boolean isOpen, byte[] pictureData, 
			String recipientFbId, String senderFbId) {
		this.caption = caption;
		this.recipientName = recipientName;
		this.senderName = senderName;
		this.dateSent = dateSent;
		this.isOpen = isOpen;
		this.pictureData = pictureData;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getRecipientName() {
		return recipientName;
	}

	public void setRecipientName(String recipient) {
		this.recipientName = recipient;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String sender) {
		this.senderName = sender;
	}

	public String getDateSent() {
		return dateSent;
	}

	public void setDateSent(String dateSent) {
		this.dateSent = dateSent;
	}

	public boolean isOpen() {
		return isOpen;
	}

	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}

	public byte[] getPictureData() {
		return pictureData;
	}

	public void setPictureData(byte[] pictureData) {
		this.pictureData = pictureData;
	}

	public String getRecipientFacebookId() {
		return recipientFacebookId;
	}

	public void setRecipientFacebookId(String recipientFacebookId) {
		this.recipientFacebookId = recipientFacebookId;
	}

	public String getSenderFacebookId() {
		return senderFacebookId;
	}

	public void setSenderFacebookId(String senderFacebookId) {
		this.senderFacebookId = senderFacebookId;
	}
}
