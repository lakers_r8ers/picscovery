package edu.calpoly.model;

import com.parse.ParseGeoPoint;

public class PicscoveryUser {
	private String m_name; 
	private int m_age;
	private ParseGeoPoint m_geoLocation;
	private String m_gender;
	private String m_facebookID;
	private String m_hometown;
	
	public PicscoveryUser() {
		this.m_age = 0;
		this.m_gender = "female";
		this.m_geoLocation = new ParseGeoPoint();
		this.m_name = "";
		this.m_facebookID = "";
	}
	
	public PicscoveryUser(String name, int age, ParseGeoPoint geoLocation, String gender, String facebookID) {
		this.m_age = age;
		this.m_gender = gender;
		this.m_geoLocation = geoLocation;
		this.m_name = name;
		this.m_facebookID = facebookID;
	}
	
	public String getName() {
		return m_name;
	}

	public void setName(String m_name) {
		this.m_name = m_name;
	}

	public int getAge() {
		return m_age;
	}

	public void setAge(int m_age) {
		this.m_age = m_age;
	}

	public ParseGeoPoint getGeoLocation() {
		return m_geoLocation;
	}

	public void setGeoLocation(ParseGeoPoint m_geoLocation) {
		this.m_geoLocation = m_geoLocation;
	}

	public String getGender() {
		return m_gender;
	}

	public void setGender(String m_gender) {
		this.m_gender = m_gender;
	}
	
	public String getFacebookID() {
		return m_facebookID;
	}

	public void setFacebookID(String id) {
		this.m_facebookID = id;
	}
	
	public void setHometown(String hometown) {
		this.m_hometown = hometown;
	}
	
	public String getHometown() {
		return this.m_hometown;
	}
}
