package edu.calpoly.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

import edu.calpoly.model.Filter;
import edu.calpoly.picscovery.R;


public class FilterFragment extends SherlockFragment implements SeekBar.OnSeekBarChangeListener, RadioGroup.OnCheckedChangeListener {
	private RadioGroup filterGroup;
	private TextView distanceTextView;
	private int milesAway;
	private String genderPreference;
	
	public View onCreateView (LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
		View filterView = inflater.inflate(R.layout.filter_layout, container, false);
		
		SeekBar seekbar = (SeekBar) filterView.findViewById(R.id.distanceSeekBar);
		seekbar.setOnSeekBarChangeListener(this);
		seekbar.setProgress(50);
		
		filterGroup = (RadioGroup)filterView.findViewById(R.id.genderRadioGroup);
		filterGroup.setOnCheckedChangeListener(this);
		
		distanceTextView = (TextView) filterView.findViewById(R.id.distanceTextView);
		distanceTextView.setText("Discover people 9 miles away");
		
		genderPreference = "both";
		milesAway = 9;
		
		return filterView;
	}
	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		if(group.equals(filterGroup)){
			if(checkedId == R.id.malePrefs) {
				genderPreference = "male";
			}
			else if(checkedId == R.id.femalePrefs) {
				genderPreference = "female";
			}
			else if(checkedId == R.id.bothPrefs) {
				genderPreference = "both";
			}
			PicscoveryApp.setFilter(new Filter(genderPreference, milesAway));
		}
	}
	
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		milesAway = progress / 5;
		milesAway = (milesAway == 0) ? 1 : milesAway;
 		distanceTextView.setText("Discover people " + milesAway + " miles away");
		PicscoveryApp.setFilter(new Filter(genderPreference, milesAway));
	}
	
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		
	} 
	
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		
	}
	
}
