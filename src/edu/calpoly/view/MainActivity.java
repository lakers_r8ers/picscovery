package edu.calpoly.view;

import java.io.File;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;


import edu.calpoly.controller.MenuListAdapter;
import edu.calpoly.model.PicscoveryUser;
import edu.calpoly.picscovery.R;

public class MainActivity extends SherlockFragmentActivity {
	final int TAKE_PICTURE = 1;
	public static final int CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE = 1777; 
	
	// UI Components
	private DrawerLayout m_drawerLayout;
	private ImageButton m_drawerButton;
	private ListView m_drawerList;
	
	// Resources for the drawer menu list items
	private String[] m_title;
	private String[] m_subtitle;
	private int[] m_icon;
	private MenuListAdapter m_menuListAdapter;
	
	private SearchFragment searchFragment = new SearchFragment();
	private MessageFragment messageFragment = new MessageFragment();
	private FilterFragment filterFragment = new FilterFragment();
	
	private boolean takingPicture;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_drawer);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		takingPicture = false;
		
		initCustomMenuBar();
		initDrawerListResources();
		initLayout();
		
		if (savedInstanceState == null) {
			selectItem(0);
		}
	}
	
	private void initDrawerListResources() {
		m_title = new String[] { 
				"Search", 
				"Messages",
				"Filter"
		};
		m_subtitle = new String[] { 
				"Start Picscovering",
				"See your picture messages",
				"Filter your search list"
		};
		m_icon = new int[] {
			R.drawable.ic_2_action_search, 
			R.drawable.ic_5_content_email, 
			R.drawable.ic_2_action_settings
		};
		
	}
	
	// Initializes all UI components
	private void initLayout() {
		m_drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		m_drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		
		m_drawerList = (ListView) findViewById(R.id.listview_drawer);
		m_drawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
	                long id) {
				selectItem(position);
			}
		});
		
		m_menuListAdapter = new MenuListAdapter(MainActivity.this, m_title, m_subtitle, m_icon);
		m_drawerList.setAdapter(m_menuListAdapter);
		
		m_drawerButton = (ImageButton) findViewById(R.id.drawerButton);
		m_drawerButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (m_drawerLayout.isDrawerOpen(m_drawerList)) {
	                m_drawerLayout.closeDrawer(m_drawerList);
	            } else {
	                m_drawerLayout.openDrawer(m_drawerList);
	            }
			}
		});
	}
	
	private void selectItem(int position) {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		
		switch (position) {
		case 0:
			ft.replace(R.id.content_frame, searchFragment);
			break;
		case 1:
			ft.replace(R.id.content_frame, messageFragment);
			break;
		case 2: 
			ft.replace(R.id.content_frame, filterFragment);
			break;
		}
		ft.commit();
		m_drawerList.setItemChecked(position, true);
		m_drawerLayout.closeDrawer(m_drawerList);
	}

	// Initialize the camera fragment
	public void startCamera() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File file = new File(Environment.getExternalStorageDirectory(),
		"my-photo.jpg");
		Uri photoPath = Uri.fromFile(file);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, photoPath); 
		
		// start camera activity
	    startActivityForResult(intent, TAKE_PICTURE);
	    
	    takingPicture = true;   
	}
	
	@Override
	protected void onPostResume() {
		super.onPostResume();
		
		if (takingPicture) {
			Intent intent = new Intent(this, MessageCreationActivity.class);
			startActivity(intent);
		}
		
		takingPicture = false;
	}
	
	private void initCustomMenuBar() {
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		View view = getLayoutInflater().inflate(R.layout.custom_action_bar, null);   
		getSupportActionBar().setCustomView(view);
	}
}
