package edu.calpoly.view;

import edu.calpoly.picscovery.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MenuItemView extends LinearLayout {
	private TextView titleView;
    private TextView subtitleView;
    private ImageView imgIcon;
	
    public MenuItemView(Context context, String title, String subtitle, int icon) {
		super(context);
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.menulist_item, this, true);
		setMenuItem(title, subtitle, icon);
	}
    
    public void setMenuItem(String title, String subtitle, int icon) {
    	titleView = (TextView) findViewById(R.id.menulistitem_title);
		titleView.setText(title);
		subtitleView = (TextView) findViewById(R.id.menulistitem_subtitle);
		subtitleView.setText(subtitle);
		imgIcon = (ImageView) findViewById(R.id.menulistitem_icon);
		imgIcon.setImageResource(icon);
    }
    
    
}
