package edu.calpoly.view;

import java.io.ByteArrayOutputStream;
import java.io.File;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockActivity;

import edu.calpoly.helper.ParseFBHelper;
import edu.calpoly.model.PicscoveryUser;
import edu.calpoly.picscovery.R;

public class MessageCreationActivity extends SherlockActivity {
	private ImageView m_image;
	private EditText m_caption;
	private Button m_sendButton;
	private Bitmap m_imageBitmap;
	private PicscoveryApp m_app;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message_creation_layout);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		getSupportActionBar().hide();
		
		// Get the Application Class Object
		m_app = (PicscoveryApp) getApplication();
		
		initLayout();
		loadImage();
	}
	
	private void initLayout() {
		m_image = (ImageView) findViewById(R.id.imageThumbnail);
		m_caption = (EditText) findViewById(R.id.imageCaption);
		m_sendButton = (Button) findViewById(R.id.sendButton);
		m_sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				sendMessage();
				startMainActivity();
			}
		});
	}
	
	private void startMainActivity() {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}
	
	private byte[] imageByteArray() {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		m_imageBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
		return stream.toByteArray();
	}
	
	private void sendMessage() {
		byte[] imageData = imageByteArray(); 
		String caption = m_caption.getText().toString();
		PicscoveryUser sender = m_app.getSender();
		PicscoveryUser recipient = m_app.getRecipient();
		ParseFBHelper.sendMessage(sender, recipient, imageData, caption); 
	}
	
	private void loadImage() {
		File file = new File(Environment.getExternalStorageDirectory(), "my-photo.jpg");
	    m_imageBitmap = decodeSampledBitmapFromFile(file.getAbsolutePath(), 300, 400);
		m_image.setImageBitmap(m_imageBitmap);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
	
	private Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) 
	 { // BEST QUALITY MATCH
	      
	     //First decode with inJustDecodeBounds=true to check dimensions
	     final BitmapFactory.Options options = new BitmapFactory.Options();
	     options.inJustDecodeBounds = true;
	     BitmapFactory.decodeFile(path, options);
	  
	     // Calculate inSampleSize, Raw height and width of image
	     final int height = options.outHeight;
	     final int width = options.outWidth;
	     options.inPreferredConfig = Bitmap.Config.RGB_565;
	     int inSampleSize = 1;
	  
	     if (height > reqHeight) 
	     {
	         inSampleSize = Math.round((float)height / (float)reqHeight);
	     }
	     int expectedWidth = width / inSampleSize;
	  
	     if (expectedWidth > reqWidth) 
	     {
	         //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
	         inSampleSize = Math.round((float)width / (float)reqWidth);
	     }
	     
	     options.inSampleSize = inSampleSize;
	  
	     // Decode bitmap with inSampleSize set
	     options.inJustDecodeBounds = false;
	  
	     return BitmapFactory.decodeFile(path, options);
	 }
}
