package edu.calpoly.view;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.SherlockFragment;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import edu.calpoly.controller.MessageListAdapter;
import edu.calpoly.helper.ParseFBHelper;
import edu.calpoly.model.Message;
import edu.calpoly.model.PicscoveryUser;
import edu.calpoly.picscovery.R;

public class MessageFragment extends SherlockFragment {
	
	// UI Components
	private View rootView;
	private ListView m_messageListView;
	private ProgressDialog m_loadingDialog;
	
	// Message List
	private ArrayList<Message> m_messageList;
	private MessageListAdapter m_messageListAdapter;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.message_list_layout, container, false);
        
        m_messageList = new ArrayList<Message>();
        m_messageListAdapter = new MessageListAdapter(rootView.getContext(), m_messageList);
        
        initLayout();
        initMessageList();
        
        return rootView;
    }
	
	private void initMessageList() {
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Messages");
		PicscoveryUser user = ParseFBHelper.parseToPicscoveryUser(ParseUser.getCurrentUser());
		query.whereEqualTo("recipientFbId", user.getFacebookID());
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
		    public void done(List<ParseObject> messageList, ParseException e) {
		        if (e == null) {
		            for (ParseObject object : messageList) {
		            	Message message = ParseFBHelper.parseToPicscoveryMessage(object);
		            	addMessage(message);
		            }
		            m_messageListView.setVisibility(View.VISIBLE);
		            m_loadingDialog.hide();
		            
		        } else {
		            Log.d(PicscoveryApp.TAG, "Error: " + e.getMessage());
		        } 
		    }
		});
	}
	
	private void initLayout() {
		m_messageListView = (ListView) rootView.findViewById(R.id.messageListViewGroup);
		m_messageListView.setAdapter(m_messageListAdapter);	
		m_messageListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
				Message m = m_messageList.get(position);
				((PicscoveryApp) getActivity().getApplication()).setMessage(m);
				Intent intent = new Intent(MessageFragment.this.getActivity(), MessageViewerActivity.class);
				startActivity(intent);
			}
		});
		m_messageListView.setVisibility(View.INVISIBLE);
		m_loadingDialog = new ProgressDialog(getActivity());
		m_loadingDialog.setMessage("Loading...");
		m_loadingDialog.setCancelable(false);
		m_loadingDialog.show();
	}
	
	private void addMessage(Message m) {
		m_messageList.add(m);
		m_messageListAdapter.notifyDataSetChanged();   
	}
}
