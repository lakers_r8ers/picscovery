package edu.calpoly.view;

import com.facebook.widget.ProfilePictureView;

import edu.calpoly.model.Message;
import edu.calpoly.picscovery.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MessageListItemView extends LinearLayout {
	private TextView m_senderName;
	private TextView m_dateSent;
	private ProfilePictureView m_profilePictureView;
	
	public MessageListItemView(Context context, Message message) {
		super(context);
		
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.message_list_item, this, true);
		
		m_senderName = (TextView) findViewById(R.id.senderName);
		m_dateSent = (TextView) findViewById(R.id.dateSent);
		m_profilePictureView = (ProfilePictureView) findViewById(R.id.userProfilePicture);
		
		setMessage(message);
	}
	
	public void setMessage(Message message) {
		m_senderName.setText(message.getSenderName());
		m_dateSent.setText(message.getDateSent());
		m_profilePictureView.setProfileId(message.getSenderFacebookId());
	}
}
