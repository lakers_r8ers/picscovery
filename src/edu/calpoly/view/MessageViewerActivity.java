package edu.calpoly.view;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;

import edu.calpoly.model.Message;
import edu.calpoly.picscovery.R;

public class MessageViewerActivity extends SherlockActivity {
	private TextView m_captionView;
	private ImageView m_imageView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message_viewer);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		getSupportActionBar().hide();
		initLayout();
		showImage();
	}
	
	private void initLayout() {
		m_captionView = (TextView) findViewById(R.id.caption);
		m_imageView = (ImageView) findViewById(R.id.imageViewer);
	}
	
	private void showImage() {
		Message m = ((PicscoveryApp) getApplication()).getMessage();
		Bitmap bitmap = BitmapFactory.decodeByteArray(m.getPictureData() , 0, m.getPictureData().length);
		m_imageView.setImageBitmap(bitmap);
		m_captionView.setText(m.getCaption());
	}
}
