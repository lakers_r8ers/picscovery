package edu.calpoly.view;

import com.parse.Parse;
import com.parse.ParseFacebookUtils;

import edu.calpoly.model.Filter;
import edu.calpoly.model.Message;
import edu.calpoly.model.PicscoveryUser;
import edu.calpoly.picscovery.R;

import android.app.Application;

public class PicscoveryApp extends Application {
	public static final String TAG = "Picscovery";
	private PicscoveryUser currentSender;
	private PicscoveryUser currentRecipient;
	private Message currentMessage;
	private static Filter filter;
	@Override
	public void onCreate() {
		super.onCreate();

		Parse.initialize(this, "iDUXmcSa30R76ayjprciiEmqEECPyrbxCoTzvXXG", "iSjjG88RyYM5GIW0tkBUyEC3HMkbHt92mrOZ6eTN");
		filter = new Filter("both", 9);
		// Set your Facebook App Id in strings.xml
		ParseFacebookUtils.initialize(getString(R.string.app_id));

	}
	
	public static Application getApplication() {
		return getApplication();
	}
	public static Filter getFilter() {
		return filter;
	}
	
	public static void setFilter(Filter newfilter) {
		filter = newfilter;
	}
	
	public void setRecipient(PicscoveryUser user) {
		this.currentRecipient = user;
	}
	
	public void setSender(PicscoveryUser user) {
		this.currentSender = user;
	}
	
	public PicscoveryUser getRecipient() {
		return currentRecipient;
	}
	
	public PicscoveryUser getSender() {
		return currentSender;
	}
	
	public Message getMessage() {
		return currentMessage;
	}
	
	public void setMessage(Message message) {
		this.currentMessage = message;
	}
}
