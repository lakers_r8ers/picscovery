package edu.calpoly.view;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;


import edu.calpoly.controller.SearchListAdapter;
import edu.calpoly.helper.ParseFBHelper;
import edu.calpoly.model.PicscoveryUser;
import edu.calpoly.picscovery.R;

public class SearchFragment extends SherlockFragment {
	
	// UI Components 
	private View rootView;
	private ListView m_userListView;
	private ProgressDialog m_loadingDialog;
	
	// Search List 
	private ArrayList<PicscoveryUser> m_userList;
	private SearchListAdapter m_searchListAdapter;
	
	// Location Services Resources
	private double m_currentLatitude;
	private double m_currentLongitude;
	private boolean m_locationFound = false;
	private LocationManager m_locationManager;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.user_list_layout, container, false); 
        
     	// Initialize search list
        m_userList = new ArrayList<PicscoveryUser>();
        m_searchListAdapter = new SearchListAdapter(rootView.getContext(), m_userList);
        
        // Initialize layout
        initLayout();
        
        // Initialize location services
        m_locationFound = false;
        initLocationServices();
        
        // Fetch Facebook user info if the session is active
// 		Session session = ParseFacebookUtils.getSession();
// 		if (session != null && session.isOpened()) {
// 			makeMeRequest();
// 		}
        
        return rootView;
    }
	
	private void initLayout() { 
		m_userListView = (ListView) rootView.findViewById(R.id.userListViewGroup);
		m_userListView.setAdapter(m_searchListAdapter);
		m_userListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View view,
					int pos, long id) { 
				PicscoveryApp pa = (PicscoveryApp) getActivity().getApplication();
				pa.setSender(ParseFBHelper.parseToPicscoveryUser(ParseUser.getCurrentUser()));
				pa.setRecipient(m_userList.get(pos));
				((MainActivity)getActivity()).startCamera();
				return true;
			}
		});
		m_loadingDialog = new ProgressDialog(getActivity());
		m_loadingDialog.setMessage("Loading...");
		m_loadingDialog.setCancelable(false);
		m_loadingDialog.show();
	}
	
	void initLocationServices() {
		LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                m_currentLatitude = location.getLatitude();
                m_currentLongitude = location.getLongitude();
                
                Log.d(PicscoveryApp.TAG, "location: " + m_currentLatitude + " " + m_currentLongitude);
                
                // Fetch Facebook user info if the session is active
        		Session session = ParseFacebookUtils.getSession();
        		if (session != null && session.isOpened()) {
        			if (!m_locationFound) {
        				makeMeRequest();
        				m_locationFound = true; 
        			}
        		}
            }

            public void onStatusChanged(String provider, int status,
                    Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
		
		m_locationManager = (LocationManager) rootView.getContext().getSystemService(Context.LOCATION_SERVICE);
	    m_locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
	    m_locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener); 
		// Hard coded longitude and latitude values
	    //m_currentLatitude = 35.1837; 
		//m_currentLongitude = 120.7256; 
	}
	
	@Override
	public void onResume() {
		super.onResume();

		ParseUser currentUser = ParseUser.getCurrentUser();
		if (currentUser != null) {
			// Check if the user is currently logged
			// and show any cached content
			queryUserList();
		} else {
			// If the user is not logged in, go to the
			// activity showing the login view.
			startLoginActivity();
		}
	}
	
	private void makeMeRequest() {
		Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
			new Request.GraphUserCallback() {
				@Override
				public void onCompleted(GraphUser user, Response response) {
					if (user != null) {
						Log.d(PicscoveryApp.TAG, "Current location: " + m_currentLatitude + " " + m_currentLongitude);  
						
						// Save the user profile info in a user property
						ParseUser currentUser = ParseFBHelper.fbToParseUser(user);
						
						// Store current user location
						ParseGeoPoint geoPoint = new ParseGeoPoint(m_currentLatitude, m_currentLongitude);
						currentUser.put("location", geoPoint);
						
						currentUser.saveInBackground(); 

						// Retrieve new list of users
						queryUserList();
					} 
					else if (response.getError() != null) {
						if ((response.getError().getCategory() == FacebookRequestError.Category.AUTHENTICATION_RETRY) || 
							(response.getError().getCategory() == FacebookRequestError.Category.AUTHENTICATION_REOPEN_SESSION)) {
							Log.d(PicscoveryApp.TAG, "The facebook session was invalidated.");
							onLogoutButtonClicked();
						} else {
							Log.d(PicscoveryApp.TAG, "Some other error: " + response.getError().getErrorMessage());
						}
					}
				}
			}); 
		request.executeAsync();
	}
	
	private void queryUserList() {
		// Create query for objects in the user list
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereNotEqualTo("username", ParseUser.getCurrentUser().getUsername());
		query.whereWithinMiles("location", ParseUser.getCurrentUser().getParseGeoPoint("location"), 
				(int)PicscoveryApp.getFilter().getDistance());
		query.setLimit(30);
		
		query.findInBackground(new FindCallback<ParseUser>() {
			public void done(List<ParseUser> objects, ParseException e) { 
				if (e == null) { 
					String gender = PicscoveryApp.getFilter().getGender();
					int filterNum; 
					
					if (gender.equals("female") || gender.equals("Female")) {
						filterNum = 2;
					}
					else if (gender.equals("male") || gender.equals("Male")) {
						filterNum = 1;
					}
					else {
						filterNum = 0;
					}
					
					switch(filterNum) {
					case 0:
						m_userList.clear();
						for (ParseUser user : objects) {
							PicscoveryUser picUser = ParseFBHelper.parseToPicscoveryUser(user);
							Log.d(PicscoveryApp.TAG, "Queried " + picUser.getName() + " age " + picUser.getAge());
							addPicscoveryUser(picUser);
						}
						break;
					case 1:
						m_userList.clear();
						for (ParseUser user : objects) {
							PicscoveryUser picUser = ParseFBHelper.parseToPicscoveryUser(user);
							Log.d(PicscoveryApp.TAG, "Queried " + picUser.getName() + " age " + picUser.getAge());
							if(picUser.getGender().equals("male") || picUser.getGender().equals("Male")) {
								addPicscoveryUser(picUser);
							}
						}
						break;
					case 2:
						m_userList.clear();
						for (ParseUser user : objects) {
							PicscoveryUser picUser = ParseFBHelper.parseToPicscoveryUser(user);
							Log.d(PicscoveryApp.TAG, "Queried " + picUser.getName() + " age " + picUser.getAge());
							if(picUser.getGender().equals("female") || picUser.getGender().equals("Female")) {
								addPicscoveryUser(picUser);
							}
						}
						break;
					}	
					m_loadingDialog.hide();
			    } 
				else {
					Log.d("Error querying user list: ", e.getMessage());
			    }
			}
		});
	}
	
	private void addPicscoveryUser(PicscoveryUser user) {
		m_userList.add(user);
		m_searchListAdapter.notifyDataSetChanged();
	}
	
	private void onLogoutButtonClicked() {
		// Log the user out
		ParseUser.logOut();

		// Go to the login view
		startLoginActivity();
	}
	
	private void startLoginActivity() {
		Intent intent = new Intent(rootView.getContext(), LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
}
