package edu.calpoly.view;

import com.facebook.widget.ProfilePictureView;

import edu.calpoly.model.PicscoveryUser;
import edu.calpoly.picscovery.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UserListItemView extends LinearLayout {
	private TextView m_usernameView;
	private ProfilePictureView m_profilePictureView;
	private TextView m_ageView;
	private TextView m_location;
	
	
	public UserListItemView(Context context, PicscoveryUser user) {
		super(context);
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.user_list_item, this, true);
		m_usernameView = (TextView) findViewById(R.id.username);
		m_profilePictureView = (ProfilePictureView) findViewById(R.id.userProfilePicture);
		m_ageView = (TextView) findViewById(R.id.userAge);
		m_location = (TextView) findViewById(R.id.userLocation);
		setPicscoveryUser(user);
	}
	
	public void setPicscoveryUser(PicscoveryUser user) {
		m_usernameView.setText(user.getName());
		m_profilePictureView.setProfileId(user.getFacebookID());
		m_ageView.setText(Integer.toString(user.getAge()));
		m_location.setText(user.getHometown());
	}
}
